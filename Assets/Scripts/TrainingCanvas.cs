﻿using UnityEngine;
using System.Collections;

public class TrainingCanvas : MonoBehaviour
{
    public Animator animator;
    public CanvasManager canvasSwitcher;

    public void Open(TrainingProgram trainingProgram)
    {
        gameObject.SetActive(true);
        StartCoroutine(StartAnimation(trainingProgram));
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    public IEnumerator StartAnimation(TrainingProgram trainingProgram)
    {
        yield return new WaitForSeconds(0.5f);

        animator.SetBool("StartPlaying", true);

        yield return StartCoroutine(VirtualClock.YieldInGameTime(20)); //yields untill in game time has gone so far, 20 in this case, should be tired to the individual training.

        animator.Stop();

        GameManager.Instance.selectedCharacter.ModifyStats(trainingProgram.modifierStats);

        Close();
    }
}
