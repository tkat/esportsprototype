﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatsCanvas : MonoBehaviour
{
    [SerializeField]
    Text characterNameTxt;

    [System.Serializable]
    public struct StatBar
    {
        public Text textValueTxt;
        public Image statBarImg;
    }

    [SerializeField]
    StatBar gameKnowlegeBar, mechanicsBar, reactionsBar, staminaBar;

    public void Open(CharacterInformation characterInformation)
    {
        gameObject.SetActive(true);

        characterNameTxt.text = characterInformation.characterName;

        gameKnowlegeBar.textValueTxt.text = characterInformation.characterStats.gameKnowlege.ToString();
        gameKnowlegeBar.statBarImg.fillAmount = characterInformation.characterStats.gameKnowlege / 100f;

        mechanicsBar.textValueTxt.text = characterInformation.characterStats.mechanics.ToString();
        mechanicsBar.statBarImg.fillAmount = characterInformation.characterStats.mechanics / 100f;

        reactionsBar.textValueTxt.text = characterInformation.characterStats.reactions.ToString();
        reactionsBar.statBarImg.fillAmount = characterInformation.characterStats.reactions / 100f;

        staminaBar.textValueTxt.text = characterInformation.characterStats.stamina.ToString();
        staminaBar.statBarImg.fillAmount = characterInformation.characterStats.stamina / 100f;
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }
}
