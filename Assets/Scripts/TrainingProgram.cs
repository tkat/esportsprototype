﻿using UnityEngine;
using System.Collections;

public class TrainingProgram : ScriptableObject
{
    public CharacterInformation.CharacterStats modifierStats;

    public int inGameMinutesToComplete;

    public int costOfTraining;
}
