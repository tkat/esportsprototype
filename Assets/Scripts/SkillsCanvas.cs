﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class SkillsCanvas : MonoBehaviour {

    public Image skillImage;
    public Image skillImage2;
    public Text skillPointsText;

    public List<GameObject> skill2;
    public List<GameObject> skill3;
    public List<Sprite> skillSprites;

    int skillLevel = 0;

    bool skillIsFull;

    Color newColor;

    void Start()
    {
        newColor = Color.white;
        newColor.a = 255;        
    }

	public void IncreaseSkillLevel()
    {
        if (!skillIsFull)
        {
            skillImage.sprite = skillSprites[skillLevel];
            
            skillLevel++;
            Debug.Log("skilllevel is " + skillLevel);
            GameManager.Instance.skillPointsAvaiable--;
            skillPointsText.text = GameManager.Instance.skillPointsAvaiable.ToString();
            if(skillLevel==6)
            {
                skillLevel = 0;
                skillIsFull = true;
                
            }
        }

        if(skillIsFull&&GameManager.Instance.skillPointsAvaiable!=0)
        {
            skillImage2.sprite = skillSprites[skillLevel];
            skillLevel++;
            GameManager.Instance.skillPointsAvaiable--;
            skillPointsText.text = GameManager.Instance.skillPointsAvaiable.ToString();
        }
    }

    void Update()
    {
        if(skillIsFull)
        {
            foreach(GameObject skill in skill2)
            {
                skill.GetComponent<Image>().color = newColor;
            }

            foreach (GameObject skill in skill3)
            {
                skill.GetComponent<Image>().color = newColor;
            }
        }
    }

    public void Open()
    {
        gameObject.SetActive(true);
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }
}
