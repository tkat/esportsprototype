﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelSelect : MonoBehaviour {

    

	public void GoToLevel(string level)
    {
        SceneManager.LoadScene(level);
    }
}
