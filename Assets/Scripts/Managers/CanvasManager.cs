﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class CanvasManager : UnitySingleton<CanvasManager>
{
    public SkillsCanvas skillsCanvas;
    public StatsCanvas statsCanvas;
    public TrainingCanvas trainingCanvas;

    public GameObject trainingSubMenu;

    public void ToggleTrainingSubMenu()
    {
        if(trainingSubMenu.activeInHierarchy)
        {
            trainingSubMenu.SetActive(false);
        }
        else
        {
            trainingSubMenu.SetActive(true);
        }
    }

    public void OpenSkillsCanvas()
    {
       skillsCanvas.Open();
    }

    public void OpenStatsPageForCharacter(CharacterInformation characterInformation)
    {
        statsCanvas.Open(characterInformation);
    }

    public void OpenTrainingScreen(TrainingProgram trainingProgram)
    {
        trainingCanvas.Open(trainingProgram);
    }
    
}
