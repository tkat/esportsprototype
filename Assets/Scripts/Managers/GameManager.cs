﻿using UnityEngine;
using System.Collections;

public class GameManager : UnitySingleton<GameManager>
{
    public int skillPointsAvaiable;

    public int currentExp;

    public int nextLevelExp;

    //not really needed for prototype, but good to have basic behaviour for when training individuals at the team stage, for now we will just hard set this to the only player
    public CharacterInformation selectedCharacter;
}
