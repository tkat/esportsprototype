﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class CharacterInformation : MonoBehaviour, IPointerClickHandler
{
    public string characterName;

    [System.Serializable]
    public struct CharacterStats
    {
        public int gameKnowlege;
        public int mechanics;
        public int stamina;
        public int reactions;

        //add the 2 stats together
        public static CharacterStats operator + (CharacterStats leftSide, CharacterStats rightSide)
        {
            leftSide.gameKnowlege += rightSide.gameKnowlege;
            leftSide.mechanics += rightSide.mechanics;
            leftSide.reactions += rightSide.reactions;
            leftSide.stamina += rightSide.stamina;
            return leftSide;
        }
    }

    public CharacterStats characterStats;

    public void OnPointerClick(PointerEventData eventData)
    {
        CanvasManager.Instance.OpenStatsPageForCharacter(this);
    }

    public void ModifyStats(CharacterStats modifyStats)
    {
        characterStats += modifyStats;
    }
}
