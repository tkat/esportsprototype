﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VirtualClock : UnitySingleton<VirtualClock>
{
    [SerializeField]
    float realSecondsInGameHour = 60;
    float realTimePerMinute;

    [SerializeField]
    Text clockDisplay;

    string clockFormat;

    bool isPM;
    int currentHour;
    int currentMinute;

    void Start()
    {
        clockFormat = clockDisplay.text;
        StartCoroutine(RunClock());
    }

    IEnumerator RunClock()
    {
        realTimePerMinute = realSecondsInGameHour / 60; //time taken for each in game minute to pass.

        while (true)
        {
            currentMinute ++;
            if(currentMinute == 60)
            {
                currentMinute = 0;
                currentHour++;

                if(currentHour == 12)
                {
                    currentHour = 1;
                    isPM = !isPM;
                }
            }

            clockDisplay.text = string.Format(clockFormat, currentHour.ToString("00"), currentMinute.ToString("00"), isPM ? "PM" : "AM");

            yield return new WaitForSeconds(realTimePerMinute);
        }
    }

    /// <summary>
    /// yield a set amount of in game time
    /// </summary>
    /// <param name="numberOfMinutes"></param>
    /// <returns></returns>
    public static IEnumerator YieldInGameTime(int numberOfMinutes)
    {
        Debug.Log("Start Event Timer");
        yield return new WaitForSeconds(numberOfMinutes * Instance.realTimePerMinute);
        Debug.Log("Event Finished");
    }
}
