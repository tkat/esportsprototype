using UnityEngine;
using System.Collections;

public class IdleRunJump : MonoBehaviour {


	protected Animator animator;
	public float DirectionDampTime = .5f;
	public bool ApplyGravity = true; 
	public float movementSpeed = 3f;

	// Use this for initialization
	void Start () 
	{
		animator = GetComponent<Animator>();
		
		/*if(animator.layerCount >= 2)
			animator.SetLayerWeight(1, 1);*/
	}
		
	// Update is called once per frame
	void Update () 
	{

		if (animator)
		{
			AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

			if (stateInfo.IsName("Base Layer.Run"))
			{

				if (Input.GetButton("Fire1")) animator.SetBool("Jump", true);
				if (Input.GetButton("Fire2")) animator.SetBool("Climb", true);
            }
			else
			{
				animator.SetBool("Jump", false);
				animator.SetBool("Climb", false);
			}
         
			/*if (Input.GetButton("Fire2")) animator.SetBool("Climb", true);                
			}

			else
			{
				animator.SetBool("Climb", false);                
			}


			if(Input.GetButtonDown("Fire2") && animator.layerCount >= 2)
			{
				animator.SetBool("Climb", !animator.GetBool("Climb"));
			}*/
			
			float h = Input.GetAxis("Horizontal");

			float v = Input.GetAxis("Vertical") * movementSpeed;


			Vector3 speed = new Vector3 (0, 0, v);

			speed = transform.rotation * speed;

			CharacterController cc = GetComponent<CharacterController> ();

			cc.SimpleMove ( speed );

			animator.SetFloat("Speed", h*h+v*v);

			animator.SetFloat("Direction", h, DirectionDampTime, Time.deltaTime);
		}   		  
	}

}